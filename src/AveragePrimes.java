/**
 * @author Yair Shpitzer
 */
public class AveragePrimes {
    /**
     * this function gets a number (represented by string)
     * and calculates the average of all the primes from 2 to this num.
     * @param args - command line arguments. valid if num is greater than 1
     */
    public static void main(String[] args) {
        if (args.length < 1) {
            System.out.println("Invalid value");
        } else {
            int maxNum = Integer.parseInt(args[0]);
            if (maxNum < 2) {
                System.out.println("Invalid value");
            } else {
                double count = 0;
                double sum = 0;
                boolean isPrime;
                for (int tempNum = 2; tempNum <= maxNum; tempNum++) {
                    isPrime = true;
                    // if the number is divided by something that is not 1 or itself then it's not prime
                    for (int i = 2; i < tempNum; i++) {
                        if (tempNum % i == 0) {
                            isPrime = false;
                            break;
                        }
                    }
                    if (isPrime) {
                        ++count;
                        sum += tempNum;
                    }
                }
                // sum / count is the average
                System.out.println(sum / count);
            }
        }
    }
}
