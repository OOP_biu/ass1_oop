/**
 * @author Yair Shpitzer
 */
public class CharCount {
    /**
     * this function gets an array of strings, and a character. then it prints all the strings that contain
     * that char. afterwards it prints all the strings that don't contain the character.
     * @param args - command line arguments. valid if there are two or more arguments, and the last is a character
     */
    public static void main(String[] args) {
        int length = args.length;
        if (length < 2) {
            System.out.println("Invalid input");
        } else {
            if (args[length - 1].length() != 1) {
                System.out.println("Invalid input");
            } else {
                char ch = args[length - 1].charAt(0);
                // print the strings that contain the character
                for (int i = 0; i < length - 1; i++) {
                    if (args[i].indexOf(ch) >= 0) {
                        System.out.println(args[i]);
                    }
                }
                // print the strings that don't contain the character
                for (int i = 0; i < length - 1; i++) {
                    if (args[i].indexOf(ch) < 0) {
                        System.out.println(args[i]);
                    }
                }
            }
        }
    }
}
