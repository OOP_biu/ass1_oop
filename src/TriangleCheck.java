/**
 * @author Yair Shpizter
 */
public class TriangleCheck {
    /**
     * this function gets three numbers (represented by strings) that are lengths of triangle edges,
     * and checks if this triangle is valid.
     * @param args - command line arguments. valid if there are three or more arguments, and all of them are positive.
     */
    public static void main(String[] args) {
        if (args.length < 3) {
            System.out.println("Invalid input");
        } else {
            double edge1 = Double.parseDouble(args[0]);
            double edge2 = Double.parseDouble(args[1]);
            double edge3 = Double.parseDouble(args[2]);
            double edge1Square = edge1 * edge1;
            double edge2Square = edge2 * edge2;
            double edge3Square = edge3 * edge3;
            boolean isRight = false;

            // all edges must be greater than 1
            if (edge1 <= 0 || edge2 <= 0 || edge3 <= 0) {
                System.out.println("Invalid input");
            } else {
                // in a triangle, the sum of any two edges must be greater than the length of third edge
                if (edge1 + edge2 <= edge3 || edge2 + edge3 <= edge1 || edge1 + edge3 <= edge2) {
                    System.out.println("not triangle");
                } else {
                    /*
                    right angled triangle must follow pythagorean's theorem (a^2 + b^2 = c^2),
                    while c is the largest edge.
                    */
                    double maxEdge = Math.max(Math.max(edge1, edge2), edge3);
                    if (maxEdge == edge1 && edge2Square + edge3Square == edge1Square) {
                        isRight = true;
                    } else if (maxEdge == edge2 && edge1Square + edge3Square == edge2Square) {
                        isRight = true;
                    } else if (maxEdge == edge3 && edge1Square + edge2Square == edge3Square) {
                        isRight = true;
                    }

                    System.out.println("triangle!");
                    if (isRight) {
                        System.out.println("right angled!");
                    }
                }
            }
        }
    }
}
